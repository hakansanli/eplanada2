import { Component, OnInit } from '@angular/core';
import { RestService } from "../services/rest.service";

declare var M;
@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  createModal:any = null;
  editModal:any = null;

  rooms:any=new Array();
  editRoomId:any=0;
  constructor(private service:RestService) { }

  ngOnInit() {
  this.loadRooms();
  }
  loadRooms(){
    this.service.getRooms().then(data=>{
      this.rooms=data;
    }).catch(()=>{
      setTimeout(this.loadRooms,500);
    });
  }
  createRoom(roomName){
    
    this.service.createRoom({name:roomName}).then(()=>{
      var elems = document.querySelectorAll('#CreateRoom');
      let createModal = M.Modal.init(elems);
      createModal[0].close();
      M.toast({html: `Kamer ${roomName} gecreeërd`});
      this.loadRooms();
    }).catch(function(){
      M.toast({html: 'Er ging iets mis!'})

    });
  }
  createRoomModal(){
    if(this.createModal==null){
      var elems = document.querySelectorAll('#CreateRoom');
      this.createModal = M.Modal.init(elems);
    }
    this.createModal[0].open();
  }
  deleteRoom(id){
    this.service.deleteRoom(id).then(()=>{
      this.loadRooms();
    }).catch(()=>{
      setTimeout(this.loadRooms,500);
    });
  }
  editRoomModal(id){
    
    
    var elems = document.querySelectorAll('#EditRoomModal'+id);
    var editModal=M.Modal.init(elems);
    editModal[0].open();
  }
  editRoom(editRoom){
    this.service.editRoom(editRoom.id,editRoom.name).then(()=>{
      this.loadRooms();
    }).catch(()=>{
      setTimeout(this.loadRooms,500);
    });
  }
}
