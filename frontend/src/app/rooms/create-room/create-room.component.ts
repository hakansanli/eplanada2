import { Component, OnInit,Output,Input,EventEmitter } from '@angular/core';

import { RestService } from "../../services/rest.service";
import { RoomsComponent } from "../rooms.component";

declare var M;
@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.css']
})
export class CreateRoomComponent implements OnInit {
  
  @Input() roomName:string;
  @Input() kind="";
  @Input() id="";
  @Output() request=new EventEmitter();
  
  constructor(private service:RestService) { }

  ngOnInit() {
    
  }
  get title(){
    return this.kind=="create"?"Creeër":"Bewerk";
  }
  submitRequest(){
    switch(this.kind){
      case "create":
        this.request.emit(this.roomName);
        break;
      case "edit":
        this.request.emit({name:this.roomName,id:this.id});
        break;
    }
  }
  

}
