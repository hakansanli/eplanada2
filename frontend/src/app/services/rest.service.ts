import { Injectable } from '@angular/core';
import  axios from "axios";
@Injectable({
  providedIn: 'root'
})
export class RestService {
token="";
  constructor() { }

  createRoom(data){
    return new Promise((resolve,reject)=>{
      this.axios.post(`${this.restUrl}insert/rooms`,data).then(()=>{
        resolve();
      }).catch(()=>{reject()});

    });
  }
  deleteRoom(id){
    return new Promise((resolve,reject)=>{
      this.axios.delete(`${this.restUrl}delete/rooms/${id}`).then((data)=>{
        resolve(data.data);
      }).catch(()=>{reject()});

    });
  }
  getRooms(){
    return new Promise((resolve,reject)=>{
      this.axios.get(`${this.restUrl}all/rooms`).then((data)=>{
        resolve(data.data);
      }).catch(()=>{reject()});

    });
  }
  getRoom(id){
    return new Promise((resolve,reject)=>{
      this.axios.get(`${this.restUrl}get/rooms/${id}`).then((data)=>{
        resolve(data.data);
      }).catch(()=>{reject()});

    });
  }
  get axios(){
    const instance = axios.create({
      headers: {
        'token': this.token
        ,'Content-type' : 'application/json'}
    });
    return instance;
  }
  get restUrl(){
    let url = this._Window.restUrl;
    console.log(url);
    url= (url==undefined)?"/":url;
    return url;
  }
  get _Window():any{
    return window;
  }
  editRoom(id,room){
    return new Promise((resolve,reject)=>{
      this.axios.put(`${this.restUrl}edit/rooms/${id}`,{"name":room}).then((data)=>{
        resolve(data.data);
      }).catch(()=>{reject()});

    });

  }
}
