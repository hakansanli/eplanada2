import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { RestService } from "../services/rest.service";

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  id:any;
  room="";
  constructor(private route:ActivatedRoute, private service:RestService) { }

  async ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.room= (await this.service.getRoom(this.id)).name;
    
  }

}
