<?php


require __DIR__.'/vendor/autoload.php';
require("functions/typevalidation.php");
require("functions/request.php");
require("functions/db.php");

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;
$router = new RouteCollector();
   
//get all
$router->get('/all/{table}', function($table){
    db::Connect();
    echo json_encode(db::GetAll($table));
    return ;
});
//get by id
$router->get("/get/{table}/{id}",function($table,$id){
    $request= Request::GetBody();
    db::Connect();
        $test= (db::GetById($table, $id));    
        echo json_encode($test);

});

//insert into
$router->post('/insert/{table}', function($table){
    $request= Request::GetBody();
    $convertedRequest = TypeValidation::ConvertType($request);
    db::Connect();
    db::InstertInto($table, $convertedRequest);
    var_dump(http_response_code(200));
    exit(200);
});

$router->put("/edit/{table}/{id}",function($table,$id){
    
    $request= Request::GetBody();
   
    db::Connect();

    db::Update($table,$id, $request);
    var_dump(http_response_code(200));
    exit(200);

});

$router->post("/where/{table}",function($table){
    $request= Request::GetBody();
    $query = $request->query;
    db::Connect();


    echo json_encode(db::GetWhere($table, $query));

});

$router->delete('/delete/{table}/{id}', function($table,$id){
    db::Connect();

    echo json_encode(db::DeleteById($table,$id));
    var_dump(http_response_code(200));
    exit(200);
    return ;
});





$dispatcher = new Phroute\Phroute\Dispatcher($router->getData());
$response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
echo $response;