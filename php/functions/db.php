<?php
require("rb.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of db
 *
 * @author hakansanli
 */
class db {
    //put your code here
    public static function Connect(){
        R::setup( 'sqlite: dbfile.db' );
      //  R::setup('mysql:host=localhost;dbname=db_espl','root','');
    }
    
    public static function InstertInto($table,$object){
        $savedata = R::dispense($table);
        foreach ($object as $key => $value) {
            $savedata->{$key}=$value;
        }
        R::store($savedata);

    }
    public static function Update($table,$id,$object){
        $savedata = R::load($table,$id);
        
        foreach ($object as $key => $value) {
            $savedata->{$key}=$value;
        }
        R::store($savedata);

    }
    
    public static function GetById($table,$id){
            return R::load( $table, $id ); 

    }
    public static function DeleteById($table,$id){
            $item = R::load( $table, $id ); 
            R::trash($item);
    }   
    
    public static function DeleteItem($item){
            R::trash($item);
    }
    
    //done
    public static function GetAll($table){
           return  R::getAll( 'SELECT * FROM '.$table);
    }
    
    //done
    public static function GetWhere($table,$query){
        return  R::getAll( 'SELECT * FROM '.$table." WHERE ".$query);
    }
}
